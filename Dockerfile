FROM ubuntu:14.04
RUN apt-get update && \
    apt-get install -y ruby

COPY hello.rb .
CMD ["ruby", "hello.rb"]
